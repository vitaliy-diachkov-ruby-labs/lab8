VARIANT = 8

$min = 0
$max = 9999

array = Array.new(VARIANT){rand($min..$max)}

puts "Array: " + array.to_s

thread_min = Thread.new {
  $min = array.min
  puts "Min: #{$min}"
}
thread_max = Thread.new {
  $max = array.max
  puts "Max: #{$max}"
}

thread_min.join
thread_max.join

thread_sum = Thread.new {
  puts "$min + max = #{$min + $max}"
}
thread_sum.join

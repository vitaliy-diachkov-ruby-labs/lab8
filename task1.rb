class Bird
  def move(seconds)
    for _ in 1..seconds
      self.print_move_message
      sleep(1)
    end
  end

  def print_move_message
    raise NotImplementedError
  end
end


class Chicken < Bird
  def print_move_message
    puts "Chicken runs as it is too young to fly"
  end
end


class Swan < Bird
  def print_move_message
    puts "Swan swims as it can't fly"
  end
end


class Penguin < Bird
  def print_move_message
    puts "Penguin swims as it can't fly"
  end
end


class Ostrich < Bird
  def print_move_message
    puts "Ostrich runs as it can't fly"
  end
end


VARIANT = 8
DELAY = VARIANT / 2


chicken = Chicken.new
swan = Swan.new
penguin = Penguin.new
ostrich = Ostrich.new


def create_thread(number, bird, delay_multiplier)
  Thread.new {
    puts "Sleeping for #{DELAY * delay_multiplier} seconds: thread delay (#{number})"
    sleep(DELAY * delay_multiplier)
    sleep(VARIANT * number)
    thread_start = Time.now
    bird.move(VARIANT)
    thread_end = Time.now
    puts "Thread #{number + 1} last for #{thread_end- thread_start} seconds"
  }
end


thread1 = Thread.new {
  thread2 = create_thread(1, swan, 3)
  thread3 = create_thread(2, penguin, 2)
  thread4 = create_thread(3, ostrich, 1)

  thread4.join
  thread3.join
  thread2.join

  sleep(DELAY * 4)

  puts "Sleeping for #{DELAY * 4} seconds: thread delay (1)"
  thread1_start = Time.now
  chicken.move(VARIANT)
  thread1_end = Time.now
  puts "Thread 1 last #{thread1_end - thread1_start} seconds"
}
thread1.join

